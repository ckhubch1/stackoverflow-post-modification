﻿using System;
using System.Reflection.Emit;

namespace StackOverflow
{
    public class Post
    {
        

        public int UpVote { get; private set; }
        public int DownVote { get; private set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DateTime { get; private set; }

        public Post()
        {
            this.DateTime = DateTime.Now;
        }

        public Post(string title)
            :this()
        {
            this.Title = title;
        }

        public Post(string title, string description)
            :this(title)
        {
            this.Description = description;
        }

        public void PrintDetails()
        {
            Console.WriteLine("Title: "+this.Title);
            Console.WriteLine("Description: " + this.Description);
            Console.WriteLine("Created on: " + this.DateTime);
            Console.WriteLine("Vote Count: " + (this.UpVote - this.DownVote));

        }

        public void UpVotePost()
        {
           this.UpVote += 1;
        }

        public void DownVotePost()
        {
            this.DownVote += 1;
        }

        public int DisplayPostVote()
        {
            return this.UpVote - this.DownVote;
        }
    }
}