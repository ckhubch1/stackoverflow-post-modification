﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackOverflow
{
    class Program
    {
        static void Main(string[] args)
        {
            var post1 = new Post();
            
            post1.Title = "MY SQL";
            post1.Description = "About MYSQL";
            var post2 = new Post();
            post2.Title = "Stupid Question";
            post2.UpVotePost();
            post1.UpVotePost();
            post1.UpVotePost();
            post1.UpVotePost();
            post2.DownVotePost();
            post2.DownVotePost();
            post2.DownVotePost();
            post2.DownVotePost();
            post2.DownVotePost();
            post1.PrintDetails();
            Console.WriteLine();
            post2.PrintDetails();


        }
    }
}
